@extends('layout.master')

@section('title', 'contact')

@section('pageTitle', 'CONTACT VIEW')

@section('body')

        <div class="d-flex justify-content-end mb-2">
            <button class="ml-1 btn btn-sm btn-outline-secondary">Pending</button>
            <button class="ml-1 btn btn-sm btn-outline-secondary">In Progress</button>
            <button class="ml-1 btn btn-sm btn-outline-secondary">Completed</button>
        </div>
        <table class="table table-bordered">
            <tr>
                <th style="width: 300px;">Name</th>
                <td>Visal</td>
            </tr>
            <tr>
                <th style="width: 300px;">Email</th>
                <td>salofficial123@gamil.com</td>
            </tr>
            <tr>
                <th style="width: 300px;">Phone Number</th>
                <td>070 871 552</td>
            </tr>
            <tr>
                <th style="width: 300px;height: 400px">Message</th>
                <td>
                    <p>Friendship is one of the most important and valuable things. All of us need friends to share our
                        thoughts and feelings, our happy and sad moments of life. Let them know how much they mean to
                        you by
                        sending them cute best friend quotes, best friend wishes and sweet cards. Lots of best friend
                        messages with images you will find on our page.</p>
                    <p>True friendship is something that lights up your darkest days. Hold on to it, because best
                        friend will always be there for you no matter what you say or do.</p>
                    <p>Friendship is a relationship in which you don’t even have to talk to each other every
                        day. But when you meet your friend after months of silence, you will understand that
                        nothing will ever change, and that true friendship is everlasting.</p>

                </td>
            </tr>
            </tbody>
        </table>

            <a href="{{url('admin/contact')}}" type="button" class="btn btn-outline-secondary btn-sm"><i class="fas fa-chevron-left"></i>BACK</a>
            <button type="button" class="btn btn-outline-secondary btn-sm float-right" data-toggle="modal" data-target="#exampleModal">REPLY</button>
            <div class="modal fade" id="exampleModal" tabindex="-1">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content text-secondary">
                        <div class="modal-header">
                            <h4 class="modal-title text-uppercase" id="exampleModalLabel">EMAIL</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form class="mb-4">
                                <div class="form-group">
                                    <label for="inputTo">To</label>
                                    <input type="text" class="form-control" id="inputTo">
                                </div>
                                <div class="form-group">
                                    <label for="inputSubject">Subject</label>
                                    <input type="text" class="form-control" id="inputSubject">
                                </div>
                                <div class="form-group">
                                    <label for="inputMessage">Message</label>
                                    <textarea style="min-height: 10rem" class="form-control" id="inputMessage"></textarea>
                                </div>
                            </form>
                            <div class="d-flex justify-content-between">
                                <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-outline-secondary">Send</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@stop
