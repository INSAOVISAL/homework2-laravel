@extends('layout.master')

@section('title', 'contact')

@section('pageTitle', 'CONTACT LIST')

@section('body')

        <div class="d-flex justify-content-end mb-3">
            <button type="button" class="btn btn-outline-secondary btn-sm float-right" data-toggle="modal" data-target="#exampleModal"><b class="text-coral">BULK EMAILS</b>
                <i class="fas fa-envelope"></i></button>

                <div class="modal fade" id="exampleModal" tabindex="-1">
                    <div class="modal-dialog modal-lg">
                        <div class="modal-content text-secondary">
                            <div class="modal-header">
                                <h4 class="modal-title text-uppercase" id="exampleModalLabel">BULK EMAILS</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form class="mb-4">
                                    <div class="form-group">
                                        <label for="inputTo">To</label>
                                        <input type="text" class="form-control" id="inputTo">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputSubject">Subject</label>
                                        <input type="text" class="form-control" id="inputSubject">
                                    </div>
                                    <div class="form-group">
                                        <label for="inputMessage">Message</label>
                                        <textarea style="min-height: 10rem" class="form-control" id="inputMessage"></textarea>
                                    </div>
                                </form>
                                <div class="d-flex justify-content-between">
                                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-outline-secondary">Send</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <form class="d-flex justify-content-between mb-2">
            <div class="d-flex">
                <div class="dropdown">
                    <button type="button" class="btn btn-outline-secondary btn-sm float-left" data-toggle="dropdown">Bulk Actions <i
                            class="fas fa-chevron-down"></i></button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" href="#">Link 1</a>
                                <a class="dropdown-item" href="#">Link 2</a>
                                <a class="dropdown-item" href="#">Link 3</a>
                            </div>
                </div>
                <div class="ml-2">
                    <button type="button"
                        class="btn btn-outline-secondary btn-sm float-left"><b>APPLY</b></button>
                </div>
                <div class="dropdown ml-2">
                    <button type="button" class=" btn btn-outline-secondary btn-sm float-right" data-toggle="dropdown">Status
                        <i class="fas fa-chevron-down"></i></button>
                        <div class="dropdown-menu">
                            <a class="dropdown-item" href="#">Link 1</a>
                            <a class="dropdown-item" href="#">Link 2</a>
                            <a class="dropdown-item" href="#">Link 3</a>
                        </div>
                </div>
                <div class="ml-2">
                    <button type="button"
                        class="btn btn-outline-secondary btn-sm float-left"><b>FILTER</b></button>
                </div>
            </div>
            <div class="d-flex">
                <input type="text" class="form-control ml-2" placeholder="Entry Keyword ...">
                <button type="button"class="btn btn-outline-secondary btn-sm ml-2"><b>SEARCH</b></button>
            </div>
        </form>
        <table class="table" >
            <thead>
              <tr>
                <th>#No</th>
                <th>Name</th>
                <th>Subject</th>
                <th>Email</th>
                <th>Status</th>
                <th>Date</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>#1</td>
                <td>Visal</td>
                <td>support</td>
                <td>salofficial123@gamil.com</td>
                <td>pending</td>
                <td>02/02/2000</td>
                <td>Edit |</td>
              </tr>
              <tr>
                <td>#1</td>
                <td>Visal</td>
                <td>support</td>
                <td>salofficial123@gamil.com</td>
                <td>pending</td>
                <td>02/02/2000</td>
                <td>Edit |</td>
              </tr>
              <tr>
                <td>#1</td>
                <td>Visal</td>
                <td>support</td>
                <td>salofficial123@gamil.com</td>
                <td>pending</td>
                <td>02/02/2000</td>
                <td>Edit |</td>
              </tr>
              <tr>
                <td>#1</td>
                <td>Visal</td>
                <td>support</td>
                <td>salofficial123@gamil.com</td>
                <td>pending</td>
                <td>02/02/2000</td>
                <td>Edit |</td>
              </tr>
              <tr>
                <td>#1</td>
                <td>Visal</td>
                <td>support</td>
                <td>salofficial123@gamil.com</td>
                <td>pending</td>
                <td>02/02/2000</td>
                <td>Edit |</td>
              </tr>
              <tr>
                <td>#1</td>
                <td>Visal</td>
                <td>support</td>
                <td>salofficial123@gamil.com</td>
                <td>pending</td>
                <td>02/02/2000</td>
                <td>Edit |</td>
              </tr>
              <tr>
                <td>#1</td>
                <td>Visal</td>
                <td>support</td>
                <td>salofficial123@gamil.com</td>
                <td>pending</td>
                <td>02/02/2000</td>
                <td>Edit |</td>
              </tr>
              <tr>
                <td>#1</td>
                <td>Visal</td>
                <td>support</td>
                <td>salofficial123@gamil.com</td>
                <td>pending</td>
                <td>02/02/2000</td>
                <td>Edit |</td>
              </tr>
            </tbody>
          </table>
          <nav class="d-flex justify-content-between text-secondary">
            <p>Total Items: <b>50</b></p>
            <ul class="pagination pagination-sm">
                <li class="page-item">
                    <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true"><i class="fa fa-chevron-left"></i></span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
                <li class="page-item ml-2"><a class="page-link" href="#">1</a></li>
                <li class="page-item ml-2"><a class="page-link" href="#">2</a></li>
                <li class="page-item ml-2"><a class="page-link" href="#">3</a></li>
                <li class="page-item ml-2"><a class="page-link" href="#">4</a></li>
                <li class="page-item ml-2"><a class="page-link" href="#">5</a></li>
                <li class="page-item ml-2">
                    <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true"><i class="fa fa-chevron-right"></i></span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
            </ul>
        </nav>
@stop
