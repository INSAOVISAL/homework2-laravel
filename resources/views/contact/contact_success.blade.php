@extends('layout.master')

@section('title', 'contact')

@section('pageTitle', 'CONTACT SUBMITED')

@section('body')

<div class="d-flex flex-column align-items-center">
    <div class="mt-5 my-5 text-center">
        <h5>Your message have been submit to our system!</h5>
        <p>Our team will feedback to you soon, Thank you</p>
    </div>
    <div class="mt-5">
        <a href="{{ url('contact') }}" class="text-uppercase btn btn-outline-secondary"><i class="fa fa-chevron-left"></i> GO BACK TO HOME PAGE</a>
    </div>
</div>

@stop
