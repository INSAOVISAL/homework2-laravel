@extends('layout.master')

@section('title', 'contact')

@section('pageTitle', 'CONTACT')

@section('body')

        <form>
            <div class="form-group">
                <label for="name">Name<span class="text-danger"> *</span></label>

                <input type="text" class="form-control" id="name" placeholder="" name="name">
            </div>
            <div class="form-group row">
                <div class="col">
                    <label for="email">Email<span class="text-danger"> *</span></label>
                    <input type="Email" class="form-control" id="email" placeholder="" name="email">
                </div>
                <div class="col">
                    <label for="phonenumber">Phone Number</label>
                    <input type="Number" class="form-control" id="phonenumber" placeholder="" name="phonenumber">
                </div>
            </div>
            <div class="form-group">
                <label for="subject">Subject</label>
                <input type="text" class="form-control" id="subject" placeholder="" name="subject">
            </div>
            <div class="form-group">
                <label for="message">Message</label>
                <textarea class="form-control" rows="10" id="message"></textarea>
              </div>
            <div>
                <a href="{{url('contact/confirm')}}">
                <button type="button" class="btn btn-outline-secondary btn-sm float-right">NEXT
                <i class="fas fa-chevron-right"></i></button>
            </div>
        </form>

@stop
