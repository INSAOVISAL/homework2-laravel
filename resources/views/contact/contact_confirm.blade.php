@extends('layout.master')

@section('title', 'contact')

@section('pageTitle', 'CONTACT CONFIRMATION')

@section('body')

        <table class="table table-bordered">
            <tr>
                <th style="width: 300px;">Name</th>
                <td>Visal</td>
            </tr>
            <tr>
                <th style="width: 300px;">Email</th>
                <td>salofficial123@gamil.com</td>
            </tr>
            <tr>
                <th style="width: 300px;">Phone Number</th>
                <td>070 871 552</td>
            </tr>
            <tr>
                <th style="width: 300px;height: 400px">Message</th>
                <td>
                    <p>Friendship is one of the most important and valuable things. All of us need friends to share our
                        thoughts and feelings, our happy and sad moments of life. Let them know how much they mean to
                        you by
                        sending them cute best friend quotes, best friend wishes and sweet cards. Lots of best friend
                        messages with images you will find on our page.</p>
                    <p>True friendship is something that lights up your darkest days. Hold on to it, because best
                        friend will always be there for you no matter what you say or do.</p>
                    <p>Friendship is a relationship in which you don’t even have to talk to each other every
                        day. But when you meet your friend after months of silence, you will understand that
                        nothing will ever change, and that true friendship is everlasting.</p>

                </td>
            </tr>
            </tbody>
        </table>
        <div>
            <a href="{{url('contact/success')}}">
                <button type="submit" class="btn btn-outline-secondary btn-sm float-right">SUBMIT</button>
        </div>
        <div>
            <a href="{{url('contact')}}">
                <button type="submit" class="btn btn-outline-secondary btn-sm"><i class="fas fa-chevron-left"></i>
                    BACK</button>
        </div>

        @stop

