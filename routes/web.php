<?php

use Facade\FlareClient\View;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/contact',function(){
    return view('contact/contacts');
});
Route::get('/contact/confirm', function () {
    return view('contact/contact_confirm');
});

Route::get('/contact/success', function () {
    return view('contact/contact_success');
});
Route::get('admin/contact', function () {
    return view('admin/contact_list');
});
Route::get('admin/contact/{id}', function ($id) {
    return view('admin/contact_view');
});

